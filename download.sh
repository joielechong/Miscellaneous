#!/bin/bash
# A simple bash script for downloading source code project for Building microservices with Go book.
counter=1
while [ $counter -le 10 ]
do
   if [ $counter -eq 10 ]
   then
     git clone https://github.com/building-microservices-with-go/chapter10-services-product
     git clone https://github.com/building-microservices-with-go/chapter10-services-auth
     git clone https://github.com/building-microservices-with-go/chapter10-services-main
    git clone https://github.com/building-microservices-with-go/chapter10-services-search
  else 
   git clone https://github.com/building-microservices-with-go/chapter$counter
   fi
   counter=$((counter + 1))

done
