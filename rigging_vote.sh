#!/bin/bash
# Rigging a voting by leveraging google chrome incognito feature

counter=1
while [ $counter -le 20 ]
do 
   # open google chrome incognito
   google-chrome-stable opensource.com/article/18/8/linux-terminal-command -incognito
  sleep 25s  # sleep waiting for user entry
  ((counter++))
done
